import { Score } from './Score';
import { TennisGame } from './TennisGame';

export class TennisGame1 implements TennisGame {
  private player1Score: number = 0;
  private player2Score: number = 0;
  private player1Name: string;
  private player2Name: string;

  constructor(player1Name: string, player2Name: string) {
    this.player1Name = player1Name;
    this.player2Name = player2Name;
  }

  wonPoint(playerName: string): void {
    if (playerName === this.player1Name)
      this.player1Score += 1;
    else
      this.player2Score += 1;
  }



  getScore(): string {
    let score: string = '';
    let playerScore = new Score(this.player1Score, this.player2Score);
    
    // Cas égalité
    if (this.player1Score === this.player2Score)
      score = playerScore.getEqualityScore();
    // Cas avantage / victoire
    else if (this.player1Score >= 4 || this.player2Score >= 4)
      score = playerScore.getAdvantageOrWinScore(this.player1Name, this.player2Name);
    // cas classique
    else
      score = playerScore.getClassicScore();

    return score;
  }
}
