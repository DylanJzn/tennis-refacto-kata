export class Score {
    private player1Score;
    private player2Score;

    constructor(player1Score: number, player2Score: number){
        this.player1Score = player1Score;
        this.player2Score = player2Score;
    }

    public getEqualityScore() {
        let score = "";
        switch (this.player1Score) {
          case 0:
            score = 'Love-All';
            break;
          case 1:
            score = 'Fifteen-All';
            break;
          case 2:
            score = 'Thirty-All';
            break;
          default:
            score = 'Deuce';
            break;
    
        }
        return score;
    }

    public getAdvantageOrWinScore(player1Name: string, player2Name: string) {
        let score = '';
        const minusResult: number = this.player1Score - this.player2Score;
    
        switch (minusResult) {
          case 1:
            score = `Advantage ${player1Name}`;
            break;
          case -1:
            score = `Advantage ${player2Name}`;
            break;
          default:
            if (minusResult >= 2)
              score = `Win for ${player1Name}`;
            else
              score = `Win for ${player2Name}`;
            break;
        }
        return score;
    }
    
    public getClassicScore() {
        let tempScore = 0;
        let score = '';
        for (let i = 1; i < 3; i++) {
          if (i === 1)
            tempScore = this.player1Score;
          else { score += '-'; tempScore = this.player2Score; }
          switch (tempScore) {
            case 0:
              score += 'Love';
              break;
            case 1:
              score += 'Fifteen';
              break;
            case 2:
              score += 'Thirty';
              break;
            case 3:
              score += 'Forty';
              break;
          }
        }
        return score;
      }
}